
GSMSps.dll: dlldata.obj GSMS_p.obj GSMS_i.obj
	link /dll /out:GSMSps.dll /def:GSMSps.def /entry:DllMain dlldata.obj GSMS_p.obj GSMS_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del GSMSps.dll
	@del GSMSps.lib
	@del GSMSps.exp
	@del dlldata.obj
	@del GSMS_p.obj
	@del GSMS_i.obj

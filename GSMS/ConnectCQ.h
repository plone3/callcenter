// ConnectCQ.h : Declaration of the CConnectCQ

#ifndef __CONNECTCQ_H_
#define __CONNECTCQ_H_

#include "resource.h"       // main symbols
#include "iads.h"
#define   GUIDINIT

/////////////////////////////////////////////////////////////////////////////
// CConnectCQ
#import "mqoa.dll"                 no_namespace
#import "..\\comsrv\\comsrv.tlb"   no_namespace
#import "..\\gsmr\\gsmr.tlb"       no_namespace
DEFINE_GUID(LIBID_MSMQ,0xD7D6E071L,0xDCCD,0x11D0,0xAA,0x4B,0x00,0x60,0x97,0x0D,0xEB,0xAE);

class ATL_NO_VTABLE CConnectCQ : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CConnectCQ, &CLSID_ConnectCQ>,
    public IDispEventImpl<0, CConnectCQ, &__uuidof(_DMSMQEventEvents), &LIBID_MSMQ, 1, 0>,
	public IDispatchImpl<IConnectCQ, &IID_IConnectCQ, &LIBID_GSMSLib>
{
public:
	CConnectCQ()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CONNECTCQ)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CConnectCQ)
	COM_INTERFACE_ENTRY(IConnectCQ)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY_IID(__uuidof(_DMSMQEventEvents),IConnectCQ)
END_COM_MAP()

/***two inherited methods of interafce IMSMQEvent***/
//这就是实现的事件接口,如果队列里有消息，这两个函数将自动被调用
STDMETHOD(ArrivedError)(IDispatch* pQueue, long lErrorCode, long lCursor);
STDMETHOD(Arrived)(IDispatch* pQueue, long lCursor);
/****************************************/
//事件接口的映像
BEGIN_SINK_MAP(CConnectCQ)
   SINK_ENTRY_EX(0, __uuidof(_DMSMQEventEvents), 0, Arrived)
   SINK_ENTRY_EX(0, __uuidof(_DMSMQEventEvents), 1, ArrivedError)
END_SINK_MAP()

HRESULT FinalConstruct()
{
	HRESULT hr = S_OK;
	hr = _OpenQueue();
	hr = _OpenGSM();
	return hr;
}
void   FinalRelease()
{
	_CloseQueue();
	_CloseGSM();
}
protected:
//send message to GSM
	HRESULT  _SendToGSM(/*[in]*/BSTR tel,BSTR msg);
//receive message from Queue
	HRESULT  _RecvFromCarQueue(/*[out]*/VARIANT*);
	HRESULT  _ParseSMS(/*[in]*/VARIANT,/*[out]*/BSTR*,/*[out]*/BSTR*);
	HRESULT  _CloseQueue();
	HRESULT  _OpenQueue();
	HRESULT  _LookupQueue(void);
	HRESULT  _MachineID(_bstr_t Name,_bstr_t* ID);
	HRESULT  _OpenGSM();
	HRESULT  _CloseGSM();
	HRESULT  _Enable();
	HRESULT  _GetDomainComputerName(LPSTR&);

protected:
	IMSMQQueueInfoPtr     m_pQueueInfo ;
    IMSMQQueuePtr         m_pQueue;
    IMSMQMessagePtr       m_pMessage;//used for sending msg to car queue
	IMSMQQueryPtr         m_pQuery;
	IMSMQApplicationPtr   m_pApp;
	IMSMQEventPtr         m_pQueueEvent;
protected:
	ISMSServerPtr         m_pSms;
	ISMArrivedSinkPtr     m_pSink;
	DWORD                 m_dwCookie;
	bool                  m_fDestructiveRead;
	_bstr_t               m_DomainName;

// IConnectCQ
public:
	STDMETHOD(GetSignal)(/*[out,ret]*/LONG *val);
};

#endif //__CONNECTCQ_H_

// ConnectCQ.cpp : Implementation of CConnectCQ
#include "stdafx.h"
#include "GSMS.h"
#include "ConnectCQ.h"
#include <string>
using namespace std;
/////////////////////////////////////////////////////////////////////////////
// CConnectCQ

#define CARQUEUE "\\CarQueueImpl"

void DispMqError(HRESULT lErrorCode)
{
	HMODULE hMQUtil = GetModuleHandle(_T("MQUTIL.DLL"));
    TCHAR* lpszMessage = NULL;
    int nChars = 0;

	if (hMQUtil != NULL) 
	{
    	nChars = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
            FORMAT_MESSAGE_FROM_HMODULE, hMQUtil, lErrorCode, 0, 
            reinterpret_cast<LPTSTR>(&lpszMessage), 1, NULL);

        if (nChars == 0) 
		{
            // Otherwise format the message with a system error message
			nChars = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
	            FORMAT_MESSAGE_FROM_SYSTEM, NULL, lErrorCode, 0, 
	            reinterpret_cast<LPTSTR>(&lpszMessage), 1, NULL);

            if (nChars == 0) 
			{
                lpszMessage = new TCHAR[512];
                // Unknown facility
                sprintf(lpszMessage, 
                    _T("Unidentified error 0x%8Xd encountered."), lErrorCode);
            }

	    }
	}

    MessageBox(GetDesktopWindow(), lpszMessage, _T("事件通知错误"), MB_OK|MB_ICONSTOP);

    // delete  the buffer alloc'ed by FormatMessage
    if (nChars != 0) 
	{
        LocalFree(lpszMessage);
    }
    else 
	{
        delete[] lpszMessage;
    }


}
HRESULT CConnectCQ::_OpenQueue()
{
	HRESULT hr = S_OK;
	TCHAR buff[512];
	LPSTR strPath;  
	hr = _GetDomainComputerName(strPath);
	_stprintf(buff,"%s%s",strPath,CARQUEUE);
	/***Open Car Queue ***/
	try
	{
		hr = m_pQueueInfo.CreateInstance(__uuidof(MSMQQueueInfo));
		_bstr_t Path  = buff;
		_bstr_t Label = "X-MMM";
		m_pQueueInfo->put_PathName(Path);
		m_pQueueInfo->put_Label(Label);		
		hr = m_pQueueEvent.CreateInstance(__uuidof(MSMQEvent));
		hr = m_pMessage.CreateInstance(__uuidof(MSMQMessage));		
		m_pQueue = m_pQueueInfo->Open(MQ_RECEIVE_ACCESS,MQ_DENY_NONE);
	}
	catch(_com_error& e)
	{
		HRESULT hr2 = e.Error();
		if(hr2==MQ_ERROR_NO_DS)
		{
			MessageBox(GetDesktopWindow(),"与服务器的Active Directory连接错误","No Active Directory",MB_OK|MB_ICONSTOP);
			return E_FAIL;
		}
		return hr2;
	}
	IDispatch* pDispTemp = NULL;
	hr = this->QueryInterface(IID_IDispatch,(void**)&pDispTemp);
	hr = AtlAdvise(m_pQueueEvent,pDispTemp,__uuidof(_DMSMQEventEvents),&m_dwCookie);
	hr = m_pQueue->EnableNotification(m_pQueueEvent);
	if(FAILED(hr))
	DispMqError(hr);
	return hr;
}
//close the queue
HRESULT CConnectCQ::_CloseQueue()
{
	if(m_pQueue!=NULL)
	{
      AtlUnadvise(m_pQueueEvent,__uuidof(IMSMQEvent),m_dwCookie);
	  m_pQueue->Close();
	}
	return S_OK;
}



HRESULT CConnectCQ::_LookupQueue(void)
{
	HRESULT hr = S_OK;
	try
	{
		hr = m_pQuery.CreateInstance(__uuidof(MSMQQuery));
		IMSMQQueueInfosPtr pTempInfos = m_pQuery->LookupQueue();
		IMSMQQueueInfoPtr  pTempInfo ;
		for(pTempInfos->Reset(),pTempInfo = pTempInfos->Next();
		pTempInfo!=NULL;pTempInfo = pTempInfos->Next())
		{
			_variant_t CreateTime ;
			pTempInfo->get_CreateTime(&CreateTime);
			_bstr_t    FormatName;
			pTempInfo->get_FormatName((BSTR*)&FormatName);
			_bstr_t    Label;
			pTempInfo->get_Label((BSTR*)&Label);
			_variant_t ModifyTime;
			pTempInfo->get_ModifyTime(&ModifyTime);
			_bstr_t PathName;
			pTempInfo->get_PathName((BSTR*)&PathName);
			_bstr_t QueueGUID;
			pTempInfo->get_QueueGuid((BSTR*)&QueueGUID);
			_bstr_t ServiceType;
			pTempInfo->get_ServiceTypeGuid((BSTR*)&ServiceType);
			LONG EncryptLevel = 0;
			pTempInfo->get_PrivLevel(&EncryptLevel);
			_bstr_t Encrypt;
			switch(EncryptLevel)
			{
			case MQ_PRIV_LEVEL_NONE:
				Encrypt = "Non-Encrypted only";break;
			case MQ_PRIV_LEVEL_BODY:
				Encrypt = "Encrypted only";break;
			case MQ_PRIV_LEVEL_OPTIONAL:
				Encrypt = "Either Encrypted is OK";break;
			default:
				Encrypt = "Unknown Encrypted type";break;	
			}
			ATLTRACE("Create Time :%s\n",CreateTime.bstrVal);
			ATLTRACE("Format Name :%s\n",FormatName);
			ATLTRACE("Label       :%s\n",Label);
			ATLTRACE("Path   Name :%s\n",PathName);
			ATLTRACE("GUID        :%s\n",QueueGUID);
			ATLTRACE("Encryption  :%s\n",Encrypt);
		}
	}
	catch(_com_error& e)
	{
		ATLTRACE("Error :\n");
		ATLTRACE("Code  :%d\n",e.Error());
        ATLTRACE("Description  :%s\n",e.ErrorMessage());
	}
	return hr;
}
HRESULT CConnectCQ::_MachineID(_bstr_t Name,_bstr_t* ID)
{
	IMSMQApplicationPtr  m_pApp;
	HRESULT hr = E_FAIL;
	m_pApp.CreateInstance(__uuidof(MSMQApplication));
	*ID = m_pApp->MachineIdOfMachineName(Name);
	return hr;
}
//消息的格式
//"13998686454#$5454456454........#"
HRESULT  CConnectCQ::_ParseSMS(/*[in]*/VARIANT var,BSTR* msg,BSTR* tel)
{
	USES_CONVERSION;
	HRESULT hr = S_OK;
	string  strOrg  = _com_util::ConvertBSTRToString(V_BSTR(&var)); 
	int pos         = strOrg.find('#');
	string  strTel  = strOrg.substr(0,pos);
	string  strMsg  = strOrg.substr(pos+1,strOrg.length()-pos-1);
	*msg   = ::SysAllocString(T2OLE(strMsg.data()));
	*tel   = ::SysAllocString(T2OLE(strTel.data()));
	return S_OK;
}

//用于从队列里接收消息
HRESULT  CConnectCQ::_RecvFromCarQueue(/*[out]*/VARIANT *res)
{
	HRESULT hr = S_OK;
	_variant_t varTemp;
	_variant_t varTimeOut;
	varTimeOut = (LONG)1000;
	ATLASSERT(m_pQueue!=NULL);
	try
	{
		m_pMessage = m_pQueue->Receive(&vtMissing,&vtMissing,
			&vtMissing,&varTimeOut);
		if(m_pMessage==NULL)
			return S_OK;
		hr = m_pMessage->get_Body(&varTemp);
		if(V_VT(&varTemp)!=VT_EMPTY)
		{
			*res = varTemp;
		}
	}
	catch(_com_error& e)
	{
		MessageBox(NULL,e.ErrorMessage(),"",MB_OK);
	}
	return hr; 
}
//打开GSM通讯
HRESULT  CConnectCQ::_OpenGSM()
{
	HRESULT hr = S_OK;
	hr = m_pSms.CreateInstance(__uuidof(SMSServer));
	ATLASSERT(m_pSms!=NULL);
	hr = m_pSms->OpenPort();
    hr = m_pSink.CreateInstance(__uuidof(ConnectWSQ));
	hr = AtlAdvise(m_pSms,m_pSink,__uuidof(ISMArrivedSink),&m_dwCookie);
	return hr;
}
//关闭GSM通讯
HRESULT  CConnectCQ::_CloseGSM()
{
	HRESULT hr = S_OK;
	hr = AtlUnadvise(m_pSms,__uuidof(ISMArrivedSink),m_dwCookie);
	if(m_pSms)
	{
		hr = m_pSms->ClosePort();
		m_pSms.Release();
		m_pSms = NULL;
	} 
	return hr;
}
//将消息发送至GSM设备
HRESULT CConnectCQ::_SendToGSM(BSTR tel,BSTR msg)
{
   HRESULT hr = S_OK;
   try
   {
	   if(m_pSms!=NULL)
	   {
    	   hr =m_pSms->put_DestinationTel(tel);
	       hr =m_pSms->SendShortMessage(msg);
	   }
	   else
	   {
		   MessageBox(GetDesktopWindow(),"Can not send message","",MB_OK|MB_ICONSTOP);
		   return E_FAIL;
	   }
   }
   catch(_com_error& e)
   {
	   HRESULT hr2 =e.Error();
	   return hr2;
   }
   return S_OK;
}
//如果被监控的队列里来了一个消息
//应该有一个分配的机制
STDMETHODIMP CConnectCQ::Arrived(IDispatch* queue,LONG cursor)
{
	ATLTRACE("A message arrived \n");
	HRESULT hr = S_OK;
	_variant_t varTemp;
	_variant_t varTimeOut;
	varTimeOut = (LONG)1000;

	ATLASSERT(m_pQueue!=NULL);
	IMSMQQueuePtr pQueueTemp;
	hr = queue->QueryInterface(__uuidof(IMSMQQueue),(void**)&pQueueTemp);
	try
	{
		m_pMessage = pQueueTemp->Receive(&vtMissing,&vtMissing,
			&vtMissing,&varTimeOut);
		if(m_pMessage==NULL)
			return E_FAIL;
		hr = m_pMessage->get_Body(&varTemp);
		//re-enable notification
		_Enable();
	}
	catch(_com_error& e)
	{
		return e.Error();
//		DISP_MQ_ERROR(e.Error());
	}
	BSTR tel,msg;
	if(V_VT(&varTemp)==VT_EMPTY)
	{
		return E_ACCESSDENIED;//no data in Car Queue
	}
	if(V_VT(&varTemp)==VT_BSTR)
	{
		_ParseSMS(varTemp,&msg,&tel);
		_SendToGSM(tel,msg);
		::SysFreeString(tel);
		::SysFreeString(msg);
		return S_OK;
	}
	return E_FAIL;
	
/*
	HRESULT hr = S_OK;
#ifdef CALLCENTERDEBUG

	MessageBox(GetDesktopWindow(),"Hello","",MB_OK);

#endif

    try 
	{
		IMSMQQueuePtr prawQueue(m_pQueue);

	    try 
		{
		    IMSMQMessagePtr pMessage = NULL;

            // Receive the message transactionally if the message is in a transactional queue
		    if (prawQueue->QueueInfo->IsTransactional == TRUE) 
			{
		        IMSMQCoordinatedTransactionDispenserPtr 
		            pXActDispenser(L"MSMQ.MSMQCoordinatedTransactionDispenser");

		        IMSMQTransactionPtr pXAct = pXActDispenser->BeginTransaction();

                VARIANT varXAct;
                varXAct.vt = VT_UNKNOWN;
                varXAct.punkVal = static_cast<IUnknown*>(pXAct);

                // Destructively read the message if requested
                if (m_fDestructiveRead) 
				{
    		        pMessage = prawQueue->ReceiveCurrent(&varXAct);
                }
                else 
				{
                    pMessage = prawQueue->PeekCurrent();
                }
		    }
		    else 
			{
                if (m_fDestructiveRead) 
				{
                    pMessage = prawQueue->ReceiveCurrent();
                }
                else 
				{
                    pMessage = prawQueue->PeekCurrent();
                }
		    }

            // Display the message
            if (pMessage != NULL) 
			{
#ifdef CALLCENTERDEBUG
                MessageBox(GetDesktopWindow(), _T("I got a message"), _T("Message arrived"), MB_OK);
#endif
			//should handle message here, for example ,send it to GSM

            }
            else 
			{
                MessageBox(GetDesktopWindow(), _T("Unexpected error."), 
                    _T("Message receive failure"), MB_OK);
            }
	    }
	    catch (_com_error eInner) 
		{
            hr = eInner.Error();
			MessageBox(GetDesktopWindow(), eInner.ErrorMessage(), 
                _T("Error retrieving message"), MB_OK);
	    }

        // Re-enable notification ,necessarily 
        _variant_t varCursor(static_cast<long>(MQMSG_NEXT));
		prawQueue->EnableNotification(m_pQueueEvent, &varCursor);
    }
    catch (_com_error eOuter) 
	{
        hr = eOuter.Error();
        MessageBox(GetDesktopWindow(), eOuter.ErrorMessage(), 
            _T("Notification enablement error"), MB_OK);
    }

	return hr;
*/
}
//错误!!!
STDMETHODIMP CConnectCQ::ArrivedError(IDispatch* pQueue, long lErrorCode, long lCursor)
{
//	return _Enable();
	HRESULT hr = S_OK;

    // Where MSMQ error strings reside
	HMODULE hMQUtil = GetModuleHandle(_T("MQUTIL.DLL"));
    TCHAR* lpszMessage = NULL;
    int nChars = 0;

	if (hMQUtil != NULL) 
	{
    	nChars = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
            FORMAT_MESSAGE_FROM_HMODULE, hMQUtil, lErrorCode, 0, 
            reinterpret_cast<LPTSTR>(&lpszMessage), 1, NULL);

        if (nChars == 0) 
		{
            // Otherwise format the message with a system error message
			nChars = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | 
	            FORMAT_MESSAGE_FROM_SYSTEM, NULL, lErrorCode, 0, 
	            reinterpret_cast<LPTSTR>(&lpszMessage), 1, NULL);

 /*           if (nChars == 0) 
			{
                lpszMessage = new TCHAR[512];
                // Unknown facility
                sprintf(lpszMessage, sizeof(lpszMessage) /sizeof(TCHAR), 
                    _T("Unidentified error 0x%8Xd encountered."), lErrorCode);
            }
*/
	    }
	}

    MessageBox(GetDesktopWindow(), lpszMessage, _T("事件通知错误"), MB_OK|MB_ICONSTOP);

    // delete  the buffer alloc'ed by FormatMessage
    if (nChars != 0) 
	{
        LocalFree(lpszMessage);
    }
    else 
	{
        delete[] lpszMessage;
    }

    // Re-enable notification
    try 
	{
		IMSMQQueuePtr prawQueue(pQueue);
        _variant_t varCursor(lCursor);
		prawQueue->EnableNotification(m_pQueueEvent, &varCursor);
    }
    catch (_com_error e) 
	{
        hr = e.Error();
        MessageBox(GetDesktopWindow(), e.ErrorMessage(), 
            _T("Notification enablement error"), MB_OK);
    }
	return S_OK;
}
HRESULT      CConnectCQ::_Enable()
{
   // Enable notification for queue using event object
   _variant_t  vtTimeout = (LONG)100000;
   _variant_t  vtCursor  ;
   V_VT(&vtCursor) = VT_I4;
   V_I4(&vtCursor) = MQMSG_CURRENT;
   HRESULT hr = E_FAIL;
   if(m_pQueue!=NULL)
   hr = m_pQueue->EnableNotification(m_pQueueEvent, &vtMissing,&vtTimeout);
   if (hr == MQ_INFORMATION_OPERATION_PENDING)
      hr = S_OK;
   return hr;
}
//得到当前域的域名
HRESULT CConnectCQ::_GetDomainComputerName(LPSTR& PDC)
{
	HRESULT hr = S_OK; 
	BSTR AnyDNSName;	
	IADsADSystemInfo *pADsys;
	CLSID clsid = __uuidof(ADSystemInfo);
	CLSID iid   = __uuidof(IADsADSystemInfo);
	try
	{
		hr = CoCreateInstance(clsid, 
			NULL, 
			CLSCTX_INPROC_SERVER, 
			iid, 
			(void**)&pADsys); 	
		hr  = pADsys->GetAnyDCName(&AnyDNSName);
		PDC = _com_util::ConvertBSTRToString(AnyDNSName);

	}
	catch(_com_error& e)
	{
		MessageBox(NULL,"Getting DNS failed",e.ErrorMessage(),MB_OK|MB_ICONSTOP);
		return e.Error();
	}
	if (pADsys) 
		pADsys->Release(); 
	return S_OK;
}

STDMETHODIMP CConnectCQ::GetSignal(LONG *val)
{
	LONG ret = 0;
	HRESULT hr = m_pSms->get_SignalQuality(&ret);
    *val = ret;
 	// TODO: Add your implementation code here

	return hr;
}


#pragma warning( disable: 4049 )  /* more than 64k source lines */

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 5.03.0280 */
/* at Fri Apr 20 17:19:30 2001
 */
/* Compiler settings for E:\CallCenterProject\GSMS\GSMS.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32 (32b run), ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __GSMS_h__
#define __GSMS_h__

/* Forward Declarations */ 

#ifndef __IConnectCQ_FWD_DEFINED__
#define __IConnectCQ_FWD_DEFINED__
typedef interface IConnectCQ IConnectCQ;
#endif 	/* __IConnectCQ_FWD_DEFINED__ */


#ifndef __ConnectCQ_FWD_DEFINED__
#define __ConnectCQ_FWD_DEFINED__

#ifdef __cplusplus
typedef class ConnectCQ ConnectCQ;
#else
typedef struct ConnectCQ ConnectCQ;
#endif /* __cplusplus */

#endif 	/* __ConnectCQ_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IConnectCQ_INTERFACE_DEFINED__
#define __IConnectCQ_INTERFACE_DEFINED__

/* interface IConnectCQ */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IConnectCQ;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("49205FFC-3515-4442-B12D-84A40A012239")
    IConnectCQ : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE Arrived( 
            /* [in] */ IDispatch __RPC_FAR *queue,
            /* [in] */ LONG cursor) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE ArrivedError( 
            /* [in] */ IDispatch __RPC_FAR *queue,
            /* [in] */ LONG cursor,
            /* [in] */ LONG cursor2) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE GetSignal( 
            /* [retval][out] */ LONG __RPC_FAR *val) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IConnectCQVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IConnectCQ __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IConnectCQ __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IConnectCQ __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IConnectCQ __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IConnectCQ __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IConnectCQ __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IConnectCQ __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Arrived )( 
            IConnectCQ __RPC_FAR * This,
            /* [in] */ IDispatch __RPC_FAR *queue,
            /* [in] */ LONG cursor);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ArrivedError )( 
            IConnectCQ __RPC_FAR * This,
            /* [in] */ IDispatch __RPC_FAR *queue,
            /* [in] */ LONG cursor,
            /* [in] */ LONG cursor2);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetSignal )( 
            IConnectCQ __RPC_FAR * This,
            /* [retval][out] */ LONG __RPC_FAR *val);
        
        END_INTERFACE
    } IConnectCQVtbl;

    interface IConnectCQ
    {
        CONST_VTBL struct IConnectCQVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IConnectCQ_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IConnectCQ_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IConnectCQ_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IConnectCQ_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IConnectCQ_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IConnectCQ_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IConnectCQ_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IConnectCQ_Arrived(This,queue,cursor)	\
    (This)->lpVtbl -> Arrived(This,queue,cursor)

#define IConnectCQ_ArrivedError(This,queue,cursor,cursor2)	\
    (This)->lpVtbl -> ArrivedError(This,queue,cursor,cursor2)

#define IConnectCQ_GetSignal(This,val)	\
    (This)->lpVtbl -> GetSignal(This,val)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IConnectCQ_Arrived_Proxy( 
    IConnectCQ __RPC_FAR * This,
    /* [in] */ IDispatch __RPC_FAR *queue,
    /* [in] */ LONG cursor);


void __RPC_STUB IConnectCQ_Arrived_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConnectCQ_ArrivedError_Proxy( 
    IConnectCQ __RPC_FAR * This,
    /* [in] */ IDispatch __RPC_FAR *queue,
    /* [in] */ LONG cursor,
    /* [in] */ LONG cursor2);


void __RPC_STUB IConnectCQ_ArrivedError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConnectCQ_GetSignal_Proxy( 
    IConnectCQ __RPC_FAR * This,
    /* [retval][out] */ LONG __RPC_FAR *val);


void __RPC_STUB IConnectCQ_GetSignal_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IConnectCQ_INTERFACE_DEFINED__ */



#ifndef __GSMSLib_LIBRARY_DEFINED__
#define __GSMSLib_LIBRARY_DEFINED__

/* library GSMSLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_GSMSLib;

EXTERN_C const CLSID CLSID_ConnectCQ;

#ifdef __cplusplus

class DECLSPEC_UUID("FE72C22D-E306-449E-9405-03D01B6088AD")
ConnectCQ;
#endif
#endif /* __GSMSLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif



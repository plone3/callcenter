// ConnectWSQ.cpp : Implementation of CConnectWSQ
#include "stdafx.h"
#include "GSMR.h"
#include "ConnectWSQ.h"

/////////////////////////////////////////////////////////////////////////////
// CConnectWSQ
#include "..\\comsrv\\comsrv.h"
#include "..\\comsrv\\comsrv_i.c"

#define WSQUEUE  "\\WorkStationQueueImpl"

HRESULT  CConnectWSQ::_GetDNS(LPSTR& PDC)
{
	HRESULT hr = S_OK; 
	BSTR AnyDNSName;	
	IADsADSystemInfo *pADsys;
	CLSID clsid = __uuidof(ADSystemInfo);
	CLSID iid   = __uuidof(IADsADSystemInfo);
	try
	{
		hr = CoCreateInstance(clsid, 
			NULL, 
			CLSCTX_INPROC_SERVER, 
			iid, 
			(void**)&pADsys); 	
		hr  = pADsys->GetAnyDCName(&AnyDNSName);
		PDC = _com_util::ConvertBSTRToString(AnyDNSName);

	}
	catch(_com_error& e)
	{
		MessageBox(NULL,"Getting DNS failed",e.ErrorMessage(),MB_OK|MB_ICONSTOP);
		return e.Error();
	}
	if (pADsys) 
		pADsys->Release(); 
	return S_OK;
}

//Purpos :当GSM收到车载机发送消息时，调用这个函数,
//var就是传送过来的消息
//形式为"+8613998696454#$62001......#"
STDMETHODIMP CConnectWSQ::MessageArrived(VARIANT var)
{
	ATLTRACE("I just received message from GSM\n");
	HRESULT hr = S_OK;
	if(V_VT(&var)==VT_EMPTY)
		return S_OK;
	_variant_t    queueMsg ;
	_variant_t    msgLabel;
//ATTENTION:
//msgLabel must be freed through using "SysFreeString" by receiver
	hr = _ParseMessage(var,&queueMsg,&msgLabel);
	if(FAILED(hr))
		return S_OK;
	hr = _SendToWSQueue(queueMsg,msgLabel);
	::SysFreeString(msgLabel.bstrVal);
	::SysFreeString(queueMsg.bstrVal);
	return  hr;
}
//Purpos: 服务器之间的通讯转换
//未加调试或验证
STDMETHODIMP CConnectWSQ::SwitchServer(BSTR path, VARIANT label)
{
	HRESULT hr = S_OK;
    m_pQueue ->Close();
    
	_bstr_t Path  = path ;
	_bstr_t Label = V_BSTR(&label);
	m_pQueueInfo->put_PathName(Path);
	m_pQueueInfo->put_Label(Label);
	try
	{
		hr = m_pQueueInfo->Create();
		if(FAILED(hr))
			_com_raise_error(hr);
		m_pQueue = m_pQueueInfo->Open(MQ_SEND_ACCESS,MQ_DENY_NONE);
	}
	catch(_com_error& e)
	{
		HRESULT hr2 = e.Error();
		if(hr2==MQ_ERROR_QUEUE_EXISTS)
		m_pQueue = m_pQueueInfo->Open(MQ_SEND_ACCESS,MQ_DENY_NONE);
	}
	hr = m_pMessage.CreateInstance(__uuidof(MSMQMessage));
	if(FAILED(hr))
	{
		m_pQueue->Close();
	}
	return hr;
}
HRESULT CConnectWSQ::_OpenQueue()
{
	HRESULT  hr = S_OK;
	TCHAR buff[512];
	LPSTR strPath;  
	hr = _GetDNS(strPath);
	_stprintf(buff,"%s%s",strPath,WSQUEUE);
	try
	{
		_bstr_t Path  = buff;
	    _bstr_t Label = "X-MMM";
		hr = m_pQueueInfo.CreateInstance(__uuidof(MSMQQueueInfo));
		hr = m_pMessage.CreateInstance(__uuidof(MSMQMessage));
		m_pQueueInfo->put_PathName(Path);
		m_pQueueInfo->put_Label(Label);
		m_pQueue = m_pQueueInfo->Open(MQ_SEND_ACCESS,MQ_DENY_NONE);
	}
	catch(_com_error& e)
	{
		MessageBox(NULL,e.ErrorMessage(),_T("Communication Service"),MB_OK|MB_ICONSTOP);
		return e.Error();
	}
	return hr;
}
//关闭队列
HRESULT CConnectWSQ::_CloseQueue()
{
	HRESULT hr   = S_OK;
	BOOL bisOpen = FALSE;
	if(m_pQueue->IsOpen==TRUE)
	{
		m_pQueue->Close();
		m_pQueue     = NULL;
        m_pQueueInfo = NULL;
		m_pMessage   = NULL;
	}
	return hr;
}

//Purpos :将msg发送至工作站队列,此消息的标签为label
HRESULT CConnectWSQ::_SendToWSQueue(VARIANT msg,VARIANT label)
{
	HRESULT hr = S_OK;
	_bstr_t myLabel = V_BSTR(&label);
	ATLASSERT(m_pMessage!=NULL&&m_pQueue!=NULL);
	if(V_VT(&msg)==VT_EMPTY)
		return E_FAIL;
	if(V_VT(&msg)==VT_BSTR)
	{
		try
		{			
			hr = m_pMessage->put_Body(msg);
			hr = m_pMessage->put_Label(myLabel);
			hr = m_pMessage->Send(m_pQueue);
		}
		catch(_com_error& e)
		{
			MessageBox(NULL,e.ErrorMessage(),_T("Send Error"),MB_OK|MB_ICONSTOP);
			return e.Error();
		}
	}
	return hr;
}

//Purpos :取得当前的系统时间
//time must be freed by SysFreeString()
HRESULT CConnectWSQ::_TimeToReceive(BSTR* time)
{
	GetSystemTime(&m_Tm);
	WCHAR tm[16];
	swprintf(tm,L"%4d%2d%2d%2d%2d%3d",m_Tm.wYear,
		m_Tm.wMonth,m_Tm.wDay,m_Tm.wHour+8,m_Tm.wMinute,
		m_Tm.wSecond,m_Tm.wMilliseconds);
	*time = SysAllocString(tm);
	return S_OK;
}

//msgIn  should be like "+8613609545332#I am here";
//varOut should be like "+8613609545332#I am here"
//Sender should be like "+8613609545332"
//Purpos :解析消息msgIn，将发送方的电话号码解析出来(Sender)
HRESULT CConnectWSQ::_ParseMessage(VARIANT msgIn,VARIANT* varOut,VARIANT* Sender)
{ 
    AFX_MANAGE_STATE(AfxGetStaticModuleState())
	CString str;
	_bstr_t bstrOrg = V_BSTR(&msgIn);
	str = (CString)_com_util::ConvertBSTRToString(bstrOrg);
	int pos = str.Find('#');
	if(pos==-1)//'#' NOT FOUND
		return E_FAIL;
	CString strTel  = str.Left(pos);
	CString strBody = str.Right(str.GetLength()-pos-1);
//	CString strBody = str;
//the length of string AT+CMGL="ALL" is 13
	if(strBody.Left(7)=="AT+CMGL")
	{
		strBody = str.Right(strBody.GetLength()-13);
	}
	if(strBody.Left(7)=="AT+CMGS")
	{
		strBody = str.Right(strBody.GetLength()-7);
	}

	_variant_t Body;
    _variant_t Tel;

	V_VT(&Body)   = VT_BSTR;
	V_BSTR(&Body) = strBody.AllocSysString();

	V_VT(&Tel)   = VT_BSTR;
	V_BSTR(&Tel) = strTel.AllocSysString();

    *varOut = Body;
   	*Sender = Tel;
   	return S_OK;

}




#pragma warning( disable: 4049 )  /* more than 64k source lines */

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 5.03.0280 */
/* at Tue Apr 03 12:18:22 2001
 */
/* Compiler settings for E:\CallCenterProject\GSMR\GSMR.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32 (32b run), ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __GSMR_h__
#define __GSMR_h__

/* Forward Declarations */ 

#ifndef __IConnectWSQ_FWD_DEFINED__
#define __IConnectWSQ_FWD_DEFINED__
typedef interface IConnectWSQ IConnectWSQ;
#endif 	/* __IConnectWSQ_FWD_DEFINED__ */


#ifndef __ConnectWSQ_FWD_DEFINED__
#define __ConnectWSQ_FWD_DEFINED__

#ifdef __cplusplus
typedef class ConnectWSQ ConnectWSQ;
#else
typedef struct ConnectWSQ ConnectWSQ;
#endif /* __cplusplus */

#endif 	/* __ConnectWSQ_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "..\\comsrv\\comsrv.h"

#ifdef __cplusplus
extern "C"{
#endif 

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IConnectWSQ_INTERFACE_DEFINED__
#define __IConnectWSQ_INTERFACE_DEFINED__

/* interface IConnectWSQ */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IConnectWSQ;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("480009C9-B2FC-4086-96DC-A2D2708D2DAF")
    IConnectWSQ : public IDispatch
    {
    public:
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE MessageArrived( 
            /* [in] */ VARIANT msg) = 0;
        
        virtual /* [id] */ HRESULT STDMETHODCALLTYPE SwitchServer( 
            /* [in] */ BSTR path,
            /* [optional][in] */ VARIANT label) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IConnectWSQVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IConnectWSQ __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IConnectWSQ __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IConnectWSQ __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            IConnectWSQ __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            IConnectWSQ __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            IConnectWSQ __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            IConnectWSQ __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *MessageArrived )( 
            IConnectWSQ __RPC_FAR * This,
            /* [in] */ VARIANT msg);
        
        /* [id] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SwitchServer )( 
            IConnectWSQ __RPC_FAR * This,
            /* [in] */ BSTR path,
            /* [optional][in] */ VARIANT label);
        
        END_INTERFACE
    } IConnectWSQVtbl;

    interface IConnectWSQ
    {
        CONST_VTBL struct IConnectWSQVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IConnectWSQ_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IConnectWSQ_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IConnectWSQ_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IConnectWSQ_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IConnectWSQ_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IConnectWSQ_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IConnectWSQ_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IConnectWSQ_MessageArrived(This,msg)	\
    (This)->lpVtbl -> MessageArrived(This,msg)

#define IConnectWSQ_SwitchServer(This,path,label)	\
    (This)->lpVtbl -> SwitchServer(This,path,label)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [id] */ HRESULT STDMETHODCALLTYPE IConnectWSQ_MessageArrived_Proxy( 
    IConnectWSQ __RPC_FAR * This,
    /* [in] */ VARIANT msg);


void __RPC_STUB IConnectWSQ_MessageArrived_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [id] */ HRESULT STDMETHODCALLTYPE IConnectWSQ_SwitchServer_Proxy( 
    IConnectWSQ __RPC_FAR * This,
    /* [in] */ BSTR path,
    /* [optional][in] */ VARIANT label);


void __RPC_STUB IConnectWSQ_SwitchServer_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IConnectWSQ_INTERFACE_DEFINED__ */



#ifndef __GSMRLib_LIBRARY_DEFINED__
#define __GSMRLib_LIBRARY_DEFINED__

/* library GSMRLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_GSMRLib;

EXTERN_C const CLSID CLSID_ConnectWSQ;

#ifdef __cplusplus

class DECLSPEC_UUID("DC664402-60CF-4523-B8FA-C1D91D254E8F")
ConnectWSQ;
#endif
#endif /* __GSMRLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long __RPC_FAR *, unsigned long            , VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long __RPC_FAR *, VARIANT __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif



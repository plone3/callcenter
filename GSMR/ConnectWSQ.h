// ConnectWSQ.h : Declaration of the CConnectWSQ

#ifndef __CONNECTWSQ_H_
#define __CONNECTWSQ_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CConnectWSQ
#include "iads.h"
#import   "mqoa.dll" no_namespace
class ATL_NO_VTABLE CConnectWSQ : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CConnectWSQ, &CLSID_ConnectWSQ>,
	public IDispatchImpl<ISMArrivedSink, &IID_ISMArrivedSink, &LIBID_COMSRVLib>,
	public IDispatchImpl<IConnectWSQ, &IID_IConnectWSQ, &LIBID_GSMRLib>
{
public:
	CConnectWSQ()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_CONNECTWSQ)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CConnectWSQ)
	COM_INTERFACE_ENTRY(IConnectWSQ)
	COM_INTERFACE_ENTRY(ISMArrivedSink)
	COM_INTERFACE_ENTRY2(IDispatch,IConnectWSQ)

END_COM_MAP()
HRESULT FinalConstruct()
{
	HRESULT hr = S_OK;
	hr = _OpenQueue();
	return hr;
	
}
VOID FinalRelease()
{
	_CloseQueue();
}
protected:
	HRESULT _OpenQueue();
	HRESULT _CloseQueue();
	HRESULT _GetDNS(LPSTR& PDC);
	HRESULT _ParseMessage(VARIANT msgIn,VARIANT* msgOut,VARIANT* Sender);
	HRESULT _SendToWSQueue(VARIANT msg,VARIANT label);
	HRESULT _TimeToReceive(BSTR* time);//the time when received message
protected:

	IMSMQQueueInfoPtr m_pQueueInfo;
	IMSMQQueuePtr     m_pQueue;
	IMSMQMessagePtr   m_pMessage;
	SYSTEMTIME        m_Tm;


// IConnectWSQ
public:
	STDMETHOD(SwitchServer)(/*[in]*/BSTR path,/*[in,optional]*/VARIANT label);
	STDMETHOD(MessageArrived)(/*[in]*/VARIANT var);
// IConnectWSQ
public:
};
static void  DispMqError(HRESULT hr)
{
	void* str; 
    HINSTANCE hInst = GetModuleHandle("mqutil.dll");
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER|
    FORMAT_MESSAGE_FROM_HMODULE,hInst,hr,NULL,(LPTSTR)&str,0,NULL);
    MessageBox(GetDesktopWindow(),(LPSTR)str,"Error",MB_OK|MB_ICONSTOP);
	LocalFree(str);
}

#endif //__CONNECTWSQ_H_

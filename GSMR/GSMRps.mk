
GSMRps.dll: dlldata.obj GSMR_p.obj GSMR_i.obj
	link /dll /out:GSMRps.dll /def:GSMRps.def /entry:DllMain dlldata.obj GSMR_p.obj GSMR_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del GSMRps.dll
	@del GSMRps.lib
	@del GSMRps.exp
	@del dlldata.obj
	@del GSMR_p.obj
	@del GSMR_i.obj
